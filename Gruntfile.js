module.exports = function (grunt) {
    grunt.initConfig({
        watch: {
            files: './scss/**/*.scss',
            tasks: ['sass']
        },
        sass: {
            dev: {
                src: './scss/main.scss',
                dest: './public/css/main.css'
            }
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src: [
                        './**/*'
                    ]
                },
                options: {
                    watchTask: true,
                    ghostMode: false,
                    server: './public',
                }
            }
        },
        uncss: {
            dist: {
                files: [
                    { src: './public/index.html', dest: './public/css/main.css' }
                ]
            }
        },
        cssmin: {
            dist: {
                
                files: [
                    { src: './public/css/main.css', dest: './public/css/main.css' }
                ]
            }
        },
        stripCssComments: {
            dist: {
                files: {
                    './public/css/main.css': './public/css/main.css'
                }
            }
        }
    });

    // load npm tasks
    grunt.loadNpmTasks('grunt-node-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-uncss');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    // define default task
    grunt.registerTask('default', ['browserSync', 'watch']);
    grunt.registerTask('build', ['uncss', 'cssmin']);
};